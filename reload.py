import os
from flask import request, Flask
app = Flask(__name__)
@app.route('/reload')
def reload():
    if request.args.get('password') == 'innopolis':
        try:
            os.system('git pull origin development')
            return 'success'
        except:
            return 'fail'
    else:
        return 'wrong password'